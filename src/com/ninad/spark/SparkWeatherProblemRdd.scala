package com.ninad.spark

import scala.util.control.Breaks
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions
import org.apache.spark.sql.Row
import java.nio.file.Path
import scala.util.Try
import org.apache.commons.io.FileUtils
import java.io.File
import org.apache.spark.sql.functions._

object SparkWeatherProblemRdd {

  val download_path = "./offline/"
  val save_path = "./hdfs/rdd/"
  val saveOutput = false

  //Creating spark session
  val sparkSession = SparkSession.builder()
    .appName("UK Weather Analysis")
    .master("local")
    .getOrCreate()

  //Download, Parse and Combine all station data
  val rdd = loadAndCombineAllStationData()

  case class Weather(station_name: String, year: Integer, month: Integer, rain: Float, sunshine: Float)

  def main(args: Array[String]): Unit = {

    FileUtils.deleteDirectory(new File(save_path))

    sparkSession.sparkContext.setLogLevel("ERROR")

    //Q1 - Rank stations by measures
    runQ1()

    //Q2.a - Rank stations by rainfall
    runQ2A()

    //Q2.b - Rank stations by sunshine
    runQ2B()

    //Q5.b - Best rain in May across all stations    
    runQ5B()

    //Q5.c - Worst rain in May across all stations  
    runQ5C()
  }

  def runQ1(): Unit = {
    //Q1 - Rank stations by measures
    val q1rdd = rdd.map(x => (x.station_name, 1))
      .reduceByKey(_ + _)
      .sortBy(_._2, false, 1)

    q1rdd.collect().foreach(println)

    if (saveOutput == true) {
      q1rdd.saveAsTextFile(save_path + "hdfs/rdd/q1")
    }
    print("[Above Output] = Rank stations by Measures\n\n")
  }

  def runQ2A(): Unit = {
    //Q2.a - Rank stations by rainfall
    val q2ardd = rdd.map(x => (x.station_name, x.rain))
      .aggregateByKey(0.0)(_ + _, _ + _)
      .sortBy(_._2, false)

    q2ardd.collect().foreach(println)

    if (saveOutput == true) {
      q2ardd.saveAsTextFile(save_path + "hdfs/rdd/q2a")
    }
    print("[Above Output] = Rank stations by rainfall\n\n")
  }

  def runQ2B(): Unit = {
    //Q2.b - Rank stations by sunshine
    val q2brdd = rdd.map(x => (x.station_name, x.sunshine))
      .aggregateByKey(0.0)(_ + _, _ + _)
      .sortBy(_._2, false)

    q2brdd.collect().foreach(println)

    if (saveOutput == true) {
      q2brdd.saveAsTextFile(save_path + "hdfs/rdd/q2b")
    }
    print("[Above Output] = Rank stations by sunshine\n\n")
  }

  def runQ5B(): Unit = {
    //Q5.b - Best rain in May across all stations    
    val q5brdd = rdd.filter(x => (x.month == 5 && x.rain > 0))
      .map(x => (x.station_name, x.rain))
      .reduceByKey((v1, v2) => Math.max(v1, v2))
      .sortBy(_._1.toString(), true, 1)

    q5brdd.collect().foreach(println)

    if (saveOutput == true) {
      q5brdd.saveAsTextFile(save_path + "hdfs/rdd/q5b")
    }
    println("[Above Output] = Best rain in May across all stations\n\n")
  }

  def runQ5C(): Unit = {
    //Q5.c - Worst rain in May across all stations  
    val q5crdd = rdd.filter(x => (x.month == 5 && x.rain > 0))
      .map(x => (x.station_name, x.rain))
      .reduceByKey((v1, v2) => Math.min(v1, v2))
      .sortBy(_._1.toString(), true, 1)

    q5crdd.collect().foreach(println)

    if (saveOutput == true) {
      q5crdd.saveAsTextFile(download_path + "hdfs/rdd/q5c")
    }
    println("[Above Output] = Worst rain in May across all stations\n\n")
  }

  def loadAndCombineAllStationData(): RDD[Weather] = {
    val stationList = List("aberporth", "armagh", "ballypatrick", "ballypatrick", "bradford", "braemar",
      "camborne", "cambridge", "cardiff", "chivenor", "cwmystwyth", "dunstaffnage", "durham", "eastbourne",
      "eskdalemuir", "heathrow", "hurn", "lerwick", "leuchars", "lowestoft", "manston", "nairn", "newtonrigg",
      "oxford", "paisley", "ringway", "rossonwye", "shawbury", "sheffield", "southampton", "stornoway", "suttonbonington",
      "tiree", "valley", "waddington", "whitby", "wickairport", "yeovilton")

    val rddArray = collection.mutable.ArrayBuffer[RDD[Weather]]()
    stationList.foreach(stationName => {
      val rdd = getWeatherData(stationName, sparkSession)
      rddArray += rdd
    })

    //union all station data together
    rddArray.reduce(_ union _)
  }

  def readWeatherFile(station: String, downloadMode: String): List[String] = {
    if (downloadMode == "online") {
      return scala.io.Source.fromURL("https://www.metoffice.gov.uk/pub/data/weather/uk/climate/stationdata/"
        + station + "data.txt").getLines().toList
    } else {
      return scala.io.Source.fromFile(download_path + "" + station + "data.txt").getLines().toList
    }
  }

  def getWeatherData(station: String, sparkSession: SparkSession): RDD[Weather] = {
    val textFileLines = readWeatherFile(station, "offline");

    //Finding the no. of lines in header
    var lastHeader = 1
    val loop = new Breaks;
    loop.breakable {
      for (line <- textFileLines) {
        lastHeader += 1
        if (line.contains("yyyy")) {
          loop.break;
        }
      }
    }

    //Removing unwanted character from lines and creating List[Row]
    val weatherDataList = textFileLines
      .slice(lastHeader, textFileLines.size)
      .map(line1 => {
        line1
          .replace("---", "000")
          .replace("*", " ")
          .replace("#", " ")
          .replace("$", "")
          .replace("Provisional", "")
          .replace("Site closed", "")
          .replace("Change to Monckton Ave", "")
          .replace("||", "")
          .replace("Site Closed", "")
          .replace("all data from Whitby", "")
      }).filter(_ != "")
      .map(line => {
        if (line.size > 42) {
          Weather(station,
            line.substring(0, 7).trim().toInt,
            line.substring(9, 13).trim().toInt,
            line.substring(35, 44).trim().toFloat,
            line.substring(44, line.size).trim().toFloat)

        } else {
          Weather(station,
            line.substring(0, 7).trim().toInt,
            line.substring(9, 13).trim().toInt,
            line.substring(35, line.size).trim().toFloat,
            "0".toFloat)
        }
      })

    //Creating and returning DataFrame from Rdd
    val rdd = sparkSession.sparkContext.parallelize(weatherDataList)
    return rdd;
  }

  def downloadFile(stationName: String) {
    try {
      val src = scala.io.Source.fromURL("https://www.metoffice.gov.uk/pub/data/weather/uk/climate/stationdata/"
        + stationName + "data.txt")
      val out = new java.io.FileWriter(download_path + "" + stationName + "data.txt")
      out.write(src.mkString)
      out.close
    } catch {
      case e: java.io.IOException => "error occured"
    }
  }
}