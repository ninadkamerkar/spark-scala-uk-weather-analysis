package com.ninad.spark

import scala.util.control.Breaks
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions
import org.apache.spark.sql.types.FloatType
import org.apache.spark.sql.types.IntegerType
import org.apache.spark.sql.types.StringType
import org.apache.spark.sql.types.StructField
import org.apache.spark.sql.types.StructType
import org.apache.commons.io.FileUtils
import java.io.File
import org.apache.spark.sql.functions._

object SparkWeatherProblemSql {

  val download_path = "./offline/"
  val save_path = "./hdfs/sql/"
  val saveOutput = false

  //Creating spark session
  val sparkSession = SparkSession.builder()
    .appName("UK Weather Analysis")
    .master("local")
    .getOrCreate()

  //Download, Parse and Combine all station data
  val df = loadAndCombineAllStationData()

  def main(args: Array[String]): Unit = {

    //Delete the directory before starting program
    FileUtils.deleteDirectory(new File(save_path))

    sparkSession.sparkContext.setLogLevel("ERROR")

    //Creating are temp table
    df.createOrReplaceTempView("station_data")

    //Q1 - Rank stations by measures
    runQ1()

    //Q2.a - Rank stations by rainfall
    runQ2A()

    //Q2.b - Rank stations by sunshine
    runQ2B()

    //Q4.a - When was the worst rainfall for each station
    runQ4A()

    //Q4.b - When was the best sunshine for each station
    runQ4B()

    //Q5 - Avg/Best/Worst rain in May across all stations  
    runQ5()
  }

  def runQ1(): Unit = {
    //Q1 - Rank stations by measures
    val q1DF = sparkSession.sqlContext.sql("SELECT station,count(station) as measures from station_data "
      + "group by station order by measures desc")

    q1DF.show(50)

    saveOutput("q1", q1DF)
    print("[Above Output] = Rank stations by measures\n\n")
  }

  def runQ2A(): Unit = {
    //Q2.a - Rank stations by rainfall
    val q2aDF = sparkSession.sqlContext.sql("SELECT station,sum(rain) as total_rain FROM station_data "
      + "where rain > 0 group by station order by total_rain desc")
    q2aDF.show(50)
    saveOutput("q2a", q2aDF)
    print("[Above Output] = Rank stations by rainfall\n\n")
  }

  def runQ2B(): Unit = {
    //Q2.b - Rank stations by sunshine
    val q2bDF = sparkSession.sqlContext.sql("SELECT station,sum(sunshine) as total_sunshine FROM station_data "
      + "where sunshine > 0 group by station order by total_sunshine desc")

    q2bDF.show(50)

    saveOutput("q2b", q2bDF)
    print("[Above Output] = Rank stations by sunshine\n\n")
  }

  def runQ4A(): Unit = {
    //Q4.a - When was the worst rainfall for each station
    val q4aDF = sparkSession.sqlContext.sql("SELECT station,year,month,rain, "
      + "RANK() OVER (partition by Station ORDER BY rain asc) AS rank FROM station_data "
      + "where rain > 0 having rank = 1 order by station asc")

    q4aDF.show(50)

    saveOutput("q4a", q4aDF)
    print("[Above Output] = When was the worst rainfall for each station\n\n")
  }

  def runQ4B(): Unit = {
    //Q4.b - When was the best sunshine for each station
    val q4bDF = sparkSession.sqlContext.sql("SELECT station,year,month,sunshine, "
      + "RANK() OVER (partition by station ORDER BY sunshine desc) AS rank FROM station_data "
      + "where sunshine > 0 having rank = 1 order by station asc")

    q4bDF.show(50)

    saveOutput("q4b", q4bDF)
    print("[Above Output] = When was the best sunshine for each station\n\n")
  }

  def runQ5(): Unit = {
    //Q5 - Avg/Best/Worst rain in May across all stations  
    val q5DF = sparkSession.sqlContext.sql("SELECT avg(rain) as avg_rain, max(rain) as max_rain,min(rain) as min_rain "
      + "from station_data where month = 5 and rain > 0")
      
    q5DF.show()
    
    saveOutput("q5", q5DF)
    println("[Above Output] = Avg/Best/Worst rain in May across all stations\n\n")
  }

  def loadAndCombineAllStationData(): DataFrame = {
    val stationList = List("aberporth", "armagh", "ballypatrick", "ballypatrick", "bradford", "braemar",
      "camborne", "cambridge", "cardiff", "chivenor", "cwmystwyth", "dunstaffnage", "durham", "eastbourne",
      "eskdalemuir", "heathrow", "hurn", "lerwick", "leuchars", "lowestoft", "manston", "nairn", "newtonrigg",
      "oxford", "paisley", "ringway", "rossonwye", "shawbury", "sheffield", "southampton", "stornoway", "suttonbonington",
      "tiree", "valley", "waddington", "whitby", "wickairport", "yeovilton")

    val dfArray = collection.mutable.ArrayBuffer[DataFrame]()
    stationList.foreach(stationName => {
      val stationDf = getWeatherData(stationName, sparkSession)
      dfArray += stationDf
    })

    //union all station data together
    dfArray.reduce(_ union _)
  }

  def saveOutput(question: String, df: DataFrame): Unit = {
    if (saveOutput == true) {
      df.repartition(1).write.csv(save_path + "hdfs/sql/" + question)
    }
  }

  def readWeatherFile(station: String, downloadMode: String): List[String] = {
    if (downloadMode == "online") {
      return scala.io.Source.fromURL("https://www.metoffice.gov.uk/pub/data/weather/uk/climate/stationdata/"
        + station + "data.txt").getLines().toList
    } else {
      return scala.io.Source.fromFile(download_path + "" + station + "data.txt").getLines().toList
    }
  }

  def getWeatherData(station: String, sparkSession: SparkSession): DataFrame = {
    val textFileLines = readWeatherFile(station, "offline");

    //Finding the no. of lines in header
    var lastHeader = 1
    val loop = new Breaks;
    loop.breakable {
      for (line <- textFileLines) {
        lastHeader += 1
        if (line.contains("yyyy")) {
          loop.break;
        }
      }
    }

    //Removing unwanted character from lines and creating List[Row]
    val weatherDataList = textFileLines
      .slice(lastHeader, textFileLines.size)
      .map(line1 => {
        line1
          .replace("---", "000")
          .replace("*", " ")
          .replace("#", " ")
          .replace("$", "")
          .replace("Provisional", "")
          .replace("Site closed", "")
          .replace("Change to Monckton Ave", "")
          .replace("||", "")
          .replace("Site Closed", "")
          .replace("all data from Whitby", "")
      }).filter(_ != "")
      .map(line => {
        if (line.size > 42) {
          Row(station,
            line.substring(0, 7).trim().toInt,
            line.substring(9, 13).trim().toInt,
            line.substring(35, 44).trim().toFloat,
            line.substring(44, line.size).trim().toFloat)

        } else {
          Row(station,
            line.substring(0, 7).trim().toInt,
            line.substring(9, 13).trim().toInt,
            line.substring(35, line.size).trim().toFloat,
            "0".toFloat)
        }
      })

    //Custom schema for weather data
    val schema = StructType(
      StructField("station", StringType, false) ::
        StructField("year", IntegerType, false) ::
        StructField("month", IntegerType, false) ::
        StructField("rain", FloatType, false) ::
        StructField("sunshine", FloatType, false) :: Nil)

    //Creating and returning DataFrame from Rdd
    return sparkSession.createDataFrame(sparkSession.sparkContext.parallelize(weatherDataList), schema)
  }

  def downloadFile(stationName: String) {
    try {
      val src = scala.io.Source.fromURL("https://www.metoffice.gov.uk/pub/data/weather/uk/climate/stationdata/"
        + stationName + "data.txt")
      val out = new java.io.FileWriter(download_path + "" + stationName + "data.txt")
      out.write(src.mkString)
      out.close
    } catch {
      case e: java.io.IOException => "error occured"
    }
  }
}